import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.openqa.selenium.json.Json;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import java.util.List;

//Check github for additional examples
//https://github.com/anshulc55/Rest_Assured_Code/blob/master/SampleRestAssured/src/rest/basic/testing/ResponseValidation.java

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;


// Sample API testing from http://apichallenges.herokuapp.com/todos

public class GetRequest_ToDos {

      public static String baseURI = "http://apichallenges.herokuapp.com";

      @BeforeTest
       public static void setup(){

            RestAssured.baseURI = baseURI;
      }

      @Test
      public void getRequest_ToDos(){
          // Just a general request to get a valid JSON object and status code
          Response response = (Response) given()
                  .contentType(ContentType.JSON)
                  .log().all()
                  .when()
                  .get("/todos")
                  .then().assertThat().statusCode(200)
                  .body("todos[0].title", notNullValue())
                  .extract().response();

          System.out.println("Request was executed ok !");

          //You could get all nodes returned from JSON response and validate each

          String strResponse = response.asString();
          JsonPath jsonResponse = new JsonPath(strResponse);

          int arrSize = jsonResponse.getInt("todos.size()");
          System.out.println("The response has: " + arrSize + " nodes");

          for (int i=0; i<arrSize; i++){
              String title = jsonResponse.getString("todos["+i+"].title");
              System.out.println("The title is: " + title);
          }

      }

    @Test
    public void getRequests_ToDos_WithParameters(){
        // Matching node value != null
        Response response = given()
                .contentType(ContentType.JSON)
                .param("title", "process payments")
                .when()
                .get("/todos")
                .then().assertThat().statusCode(200)
                .body("todos[0].title", notNullValue())
                .extract().response();

        //Get response, parse into String then JSON
        String responseBody = response.asString();
        JsonPath jsonResponse = new JsonPath(responseBody);
        System.out.println("Element title in response is: " + jsonResponse.get("todos[0].title"));
        int id = jsonResponse.get("todos[0].id");
        System.out.println("If you were an E2E scenario, you could grab the value and assign it global variable for further consumption: " + id);
        System.out.println("Request was executed ok !");


    }

}
